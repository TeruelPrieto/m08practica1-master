package com.example.practica1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.time.Instant;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnTabaco = findViewById(R.id.btnTabaco);
        Button btnRunning = findViewById(R.id.btnRunning);

        btnTabaco.setOnClickListener(this);
        btnRunning.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {

        if (view.getId()==R.id.btnTabaco) {
            Intent intentTabaco = new Intent(this, CalculadorConsumoTabaco.class);
            startActivity(intentTabaco);
        } else if (view.getId()==R.id.btnRunning) {
            Intent intentRunning = new Intent(this, CalculadorRitmoRunning.class);
            startActivity(intentRunning);
        }

    }
}
