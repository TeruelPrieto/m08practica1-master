package com.example.practica1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import static java.util.Calendar.*;

public class CalculadorConsumoTabaco extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculador_consumo_tabaco);
        Button btnConsumo = findViewById(R.id.btnConsumo);
        btnConsumo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        EditText etPrecio = findViewById(R.id.etPrecio);
        EditText etCuantos = findViewById(R.id.etCuantos);
        DatePicker dpDesdeCuando = findViewById(R.id.dpDesdeCuando);
        TextView tvAnual = findViewById(R.id.tvGastoAnual);
        TextView tvTotal = findViewById(R.id.tvGastoTotal);

        Double precio;
        String precioT = etPrecio.getText().toString();
        if (!precioT.isEmpty()) {
            precio = Double.parseDouble(precioT);
        } else {
            precio = 0.0;
        }

        Double cuantos;
        String cuantosT = etCuantos.getText().toString();
        if (!cuantosT.isEmpty()) {
            cuantos = Double.parseDouble(cuantosT);
        } else {
            cuantos = 0.0;
        }

        Date desde = getDate(dpDesdeCuando.getYear(), dpDesdeCuando.getMonth(), dpDesdeCuando.getDayOfMonth());
        Date hoy = new Date();
        long days = (hoy.getTime() - desde.getTime()) / 1000 / 60 / 60 / 24;

        double gastoAnual = (((precio/20) * cuantos * days) / (days/365.25));
        double gastoTotal = ((precio/20) * cuantos * days);

        DecimalFormat df = new DecimalFormat("0.00");
        tvAnual.setText("Gasto anual: " + df.format(gastoAnual) + "€");
        tvAnual.setVisibility(View.VISIBLE);
        tvTotal.setText("Gasto total: " + df.format(gastoTotal) + "€");
        tvTotal.setVisibility(View.VISIBLE);

    }

    public static Date getDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
