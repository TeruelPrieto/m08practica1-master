package com.example.practica1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

public class CalculadorRitmoRunning extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculador_ritmo_running);
        Button btnRitmo = findViewById(R.id.btnRitmo);
        btnRitmo.setOnClickListener(this);
        ImageView ivFoto = findViewById(R.id.ivFoto);
        ivFoto.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onClick(View view) {

        EditText etMin = findViewById(R.id.etMin);
        EditText etSec = findViewById(R.id.etSec);
        EditText etDis = findViewById(R.id.etDistancia);
        TextView tvRitme = findViewById(R.id.tvRitme);
        ImageView ivFoto = findViewById(R.id.ivFoto);

        boolean sortir = false;

        Double min;
        String minT = etMin.getText().toString();
        if (!minT.isEmpty()) {
            min = Double.parseDouble(minT);
        } else {
            min = 0.0;
        }

        Double sec;
        String secT = etSec.getText().toString();
        if (!secT.isEmpty()) {
            sec = Double.parseDouble(secT);
        } else {
            sec=0.0;
        }

        Double dis;
        String disT = etDis.getText().toString();
        if (!disT.isEmpty()) {
            dis = Double.parseDouble(disT);
        } else {
            dis=0.0;
        }

        if ((min >= 0)&&(sec >= 0 && sec < 60)&&(dis > 0)) {
            Double temps = min*60 + sec;
            Double ritme = dis / (temps/60/60);

            DecimalFormat df = new DecimalFormat("0.00");
            tvRitme.setText("Ritme:" + df.format(ritme) + "KM/h");
            tvRitme.setVisibility(View.VISIBLE);

            if (ritme < 5) {
                ivFoto.setImageResource(R.drawable.caracol);
            } else if (ritme >= 5 && ritme < 10) {
                ivFoto.setImageResource(R.drawable.conejo);
            } else if (ritme >= 10 && ritme < 20) {
                ivFoto.setImageResource(R.drawable.lince);
            } else {
                ivFoto.setImageResource(R.drawable.usain);
            }

            ivFoto.setVisibility(View.VISIBLE);


        } else {
            //Pop up que recordi que els camps no són correctes
        }



    }
}
